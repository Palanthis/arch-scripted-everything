First install ccache:

sudo pacman -S ccache

Next lets enable ccache and set our makeflags for makepkg:

sudo nano /etc/makepkg.conf

find BUILDENV=

remove the ! in front of ccache so the line looks like this:

BUILDENV=(!distcc color ccache check !sign)

find MAKEFLAGS=

change it so it looks similar to:

MAKEFLAGS="-j9 -l8"

replace 9 with your number of cores +1, and 8 with your number of cores. Save and close.

Next we need to make sure ccache and makeflags are set at all times in case we compile something without using a package manager:

nano ~/.bashrc

add these lines, save, close:

export PATH="/usr/lib/ccache/bin/:$PATH"

export MAKEFLAGS="-j9 -l8"

again, replace 9 with your number of cores +1, and 8 with your number of cores.
