#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

echo "xserver setup"
echo "You need to edit this file with nano and uncomment the appropriate video driver(s)."
echo "Also esnure you have run the root script first!"
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

# Xorg Core
sudo pacman -S xorg-server xorg-apps xorg-xinit xorg-twm xorg-xclock xterm --noconfirm --needed
sudo pacman -S linux-headers --noconfirm --needed

# Uncomment the below lines for NVidia Drivers
# sudo pacman -S nvidia-dkms nvidia-settings nvidia-utils lib32-nvidia-utils --noconfirm --needed
# sudo pacman -S lib32-opencl-nvidia opencl-nvidia libvdpau lib32-libvdpau --noconfirm --needed
# sudo pacman -S libxnvctrl --noconfirm --needed
 
# Uncomment the below line for NVidia Drivers (non-pasqual)
# sudo pacman -S nvidia-340xx-dkms nvidia-340xx-utils lib32-nvidia-340xx-utils --noconfirm --needed

# Uncomment the below line for VirtualBox
# sudo pacman -S mesa virtualbox-guest-utils --noconfirm --needed

# Uncomment the below line for Intel Video
# sudo pacman -S mesa xf86-video-intel --noconfirm --needed

# Uncomment the below line for Radeon Drivers (untested)
# sudo pacman -S vulkan-radeon lib32-vulkan-radeon --noconfirm --needed

# Use all cores for make and compress
sudo ./use-all-cores-makepkg.sh

# Network
sudo pacman -S networkmanager --noconfirm --needed
sudo pacman -S network-manager-applet --noconfirm --needed
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# Optional WiFi Drivers
# sudo pacman -S broadcom-wl-dkms --noconfirm --needed

# Install yaourt
sudo pacman -S --noconfirm --needed yaourt

# Install pamac and package-query
yaourt -S --needed --noconfirm pamac-aur

# Install build tools
sudo pacman -S --needed --noconfirm cmake ccache extra-cmake-modules
sudo pacman -S --needed --noconfirm archiso wget git

# Plasma from Standard Repos
sudo pacman -S --needed --noconfirm aspell-en bluedevil breeze
sudo pacman -S --needed --noconfirm breeze-gtk breeze-kde4 cdrdao
sudo pacman -S --needed --noconfirm discover dolphin-plugins k3b
sudo pacman -S --needed --noconfirm drkonqi gwenview ffmpegthumbs
sudo pacman -S --needed --noconfirm dvd+rw-tools kde-gtk-config
sudo pacman -S --needed --noconfirm kdebase-meta kdeconnect kfind
sudo pacman -S --needed --noconfirm kdegraphics-thumbnailers kopete
sudo pacman -S --needed --noconfirm kdelibs4support kdeplasma-addons
sudo pacman -S --needed --noconfirm kdeutils-meta kdialog kgamma5
sudo pacman -S --needed --noconfirm khelpcenter kinfocenter konsole
sudo pacman -S --needed --noconfirm kipi-plugins kmenuedit kscreen
sudo pacman -S --needed --noconfirm ksshaskpass ksysguard kwin milou
sudo pacman -S --needed --noconfirm kwayland-integration kwrited
sudo pacman -S --needed --noconfirm kwalletmanager okular oxygen
sudo pacman -S --needed --noconfirm nm-connection-editor user-manager
sudo pacman -S --needed --noconfirm kwallet-pam sweeper sddm-kcm
sudo pacman -S --needed --noconfirm packagekit-qt5 plasma-pa sddm
sudo pacman -S --needed --noconfirm partitionmanager plasma-desktop
sudo pacman -S --needed --noconfirm plasma-nm plasma-sdk spectacle
sudo pacman -S --needed --noconfirm plasma-vault powerdevil
sudo pacman -S --needed --noconfirm plasma-workspace qtcurve-gtk2
sudo pacman -S --needed --noconfirm plasma-workspace-wallpapers
sudo pacman -S --needed --noconfirm qtcurve-kde qtcurve-qt4
sudo pacman -S --needed --noconfirm qtcurve-utils systemsettings
sudo pacman -S --needed --noconfirm kvantum-qt5 kvantum-theme-adapta
sudo pacman -S --needed --noconfirm adapta-gtk-theme 
sudo pacman -S --needed --noconfirm plasma5-applets-weather-widget

# Plasma from AUR
yaourt -S --needed --noconfirm kdesudo
yaourt -S --needed --noconfirm pamac-tray-appindicator

# Enable Display Manager - comment out if keeping current DM
sudo systemctl enable sddm.service

# Sound
sudo pacman -S --noconfirm --needed pulseaudio pulseaudio-alsa pavucontrol
sudo pacman -S --noconfirm --needed alsa-utils alsa-plugins alsa-lib alsa-firmware
sudo pacman -S --noconfirm --needed gst-plugins-good gst-plugins-bad gst-plugins-base
sudo pacman -S --noconfirm --needed gst-plugins-ugly gstreamer

# Fonts from 'normal' repositories
sudo pacman -S --noconfirm --needed noto-fonts noto-fonts-emoji
sudo pacman -S --noconfirm --needed adobe-source-code-pro-fonts

# Apps from standard repos
sudo pacman -S --noconfirm --needed chromium geany geany-plugins keepass
sudo pacman -S --noconfirm --needed conky conky-manager file-roller evince
sudo pacman -S --noconfirm --needed uget deluge gparted vlc phonon-qt4-vlc
sudo pacman -S --noconfirm --needed qterminal screenfetch phonon-qt5-vlc
sudo pacman -S --noconfirm --needed cairo-dock cairo-dock-plug-ins
sudo pacman -S --noconfirm --needed asunder vorbis-tools libogg lib32-libogg
sudo pacman -S --noconfirm --needed liboggz youtube-dl

# Apps from AUR
yaourt -S --noconfirm --needed chromium-widevine
yaourt -S --noconfirm --needed gitkraken
yaourt -S --noconfirm --needed caffeine-ng

# Optional - install xdg-user-dirs and update my home directory.
#This creates the usual folders, Documents, Pictures, etc.
sudo pacman -S xdg-user-dirs --noconfirm --needed
xdg-user-dirs-update --force

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d ~/.local/share/templates ] || sudo mkdir -p ~/.local/share/templates
sudo tar xzf tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf tarballs/buuf-icons.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf tarballs/palanthis.tar.gz -C ~/ --overwrite
sudo tar xzf tarballs/templates.tar.gz -C ~/.local/share/ --overwrite

# Copy over GRUB theme
sudo tar xzf tarballs/arch-silence.tar.gz -C /boot/grub/themes/ --overwrite

# Copy over SDDM theme
sudo tar xzf tarballs/archlinux-sddm-theme.tar.gz -C /usr/share/sddm/themes/ --overwrite

# Copy SDDM config
sudo tar xzf tarballs/sddm-conf.tar.gz -C /etc/ --overwrite

# Install wallpapers
[ -d ~/Wallpapers ] || mkdir ~/Wallpapers
[ -d ~/StarWarsWallpapers ] || mkdir ~/StarWarsWallpapers
tar xzf tarballs/wallpapers1.tar.gz -C ~/Wallpapers/
tar xzf tarballs/wallpapers2.tar.gz -C ~/Wallpapers/
tar xzf tarballs/starwarswallpapers.tar.gz -C ~/StarWarsWallpapers/

# Install Conky
[ -d ~/.conky ] || mkdir ~/.conky
tar xzf tarballs/conky.tar.gz -C ~/.conky/

# Add screenfetch to .bashrc
echo screenfetch >> ~/.bashrc

echo " "
echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."

sudo reboot
