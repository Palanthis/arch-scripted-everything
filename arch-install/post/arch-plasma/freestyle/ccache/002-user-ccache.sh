#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo "This script is ONLY for an 8 core CPU, but you can edit"
echo "it for yours!"

# Add lines to .bashrc
echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
echo 'export MAKEFLAGS="-j9 -l8"' >> ~/.bashrc
