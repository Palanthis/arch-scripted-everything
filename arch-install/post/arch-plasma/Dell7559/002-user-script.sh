#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Add lines to .bashrc
echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
echo 'export MAKEFLAGS="-j5 -l4"' >> ~/.bashrc

# Xorg Core
sudo pacman -S xorg-server xorg-apps xorg-xinit xorg-twm xorg-xclock xterm --noconfirm --needed
sudo pacman -S linux-headers --noconfirm --needed

# NVidia Drivers
sudo pacman -S nvidia nvidia-settings nvidia-utils lib32-nvidia-utils --noconfirm --needed
sudo pacman -S lib32-opencl-nvidia opencl-nvidia  --noconfirm --needed
sudo pacman -S libxnvctrl libvdpau lib32-libvdpau --noconfirm --needed

# Intel Driver
sudo pacman -S --noconfirm --needed mesa
#xf86-video-intel
sudo pacman -S --noconfirm --needed libva-intel-driver intel-media-driver

# Bumblebee
sudo pacman -S --noconfirm --needed bumblebee primus lib32-primus bbswitch
sudo sytemctl enable bumblebee.service
sudo gpasswd -a palanthis bumblebee

# Bluetooth
sudo pacman -S --noconfirm --needed bluez bluez-utils
sudo systemctl enable bluetooth.service



# Call common script
sh ../003-common-script.sh
